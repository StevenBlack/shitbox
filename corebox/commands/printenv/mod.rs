use std::env;

use super::Cmd;
use clap::{Arg, ArgAction, Command};

#[derive(Debug)]
pub struct Printenv;

impl Cmd for Printenv {
    fn cli(&self) -> clap::Command {
        Command::new("printenv")
            .about("print all or part of environment")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([
                Arg::new("null")
                    .short('0')
                    .long("null")
                    .help("end each output line with NUL, not newline")
                    .action(ArgAction::SetTrue),
                Arg::new("var")
                    .value_name("VARIABLE")
                    .num_args(1..)
                    .required(false),
            ])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(vars) = matches.get_many::<String>("var") {
            for var in vars {
                let val = env::var(var)?;
                if matches.get_flag("null") {
                    print!("{var}={val}\0");
                } else {
                    println!("{var}={val}");
                }
            }
        } else {
            env::vars().for_each(|(var, val)| {
                if matches.get_flag("null") {
                    print!("{var}={val}\0");
                } else {
                    println!("{var}={val}");
                }
            });
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
