use super::Cmd;
use clap::{value_parser, Arg, ArgMatches, Command};
use std::{
    error::Error,
    io::{self, BufRead},
};

#[derive(Debug)]
pub struct Factor;

impl Cmd for Factor {
    fn cli(&self) -> Command {
        Command::new("factor")
            .about("factor numbers")
            .after_help(
                "Print the prime factors of each specified integer NUMBER.  If none are\n\
                specified on the command line, read them from standard input.",
            )
            .arg(
                Arg::new("number")
                    .help("the numbers to factor")
                    .num_args(0..)
                    .value_parser(value_parser!(u64)),
            )
    }

    fn run(&self, matches: &ArgMatches) -> Result<(), Box<dyn Error>> {
        match matches.get_many::<u64>("number") {
            Some(numbers) => {
                numbers.for_each(|n| print_factors(*n));
            }
            None => {
                for line in io::stdin().lock().lines() {
                    for num in line?.split_whitespace() {
                        print_factors(num.parse()?);
                    }
                }
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}

fn first_factor(num: u64) -> u64 {
    if num % 2 == 0 {
        return 2;
    }
    for n in (3..=num).step_by(1) {
        if num % n == 0 {
            return n;
        }
    }
    num
}

fn print_factors(num: u64) {
    print!("{num}:");
    let mut n = num;
    loop {
        let f = first_factor(n);
        print!(" {f}");
        if f == n {
            break;
        }
        n /= f;
    }
    println!();
}
