use super::Cmd;
use clap::Command;
use shitbox::args;
use std::{
    fs::File,
    io::{self, BufRead, BufReader, Write},
};
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

#[derive(Debug)]
pub struct Rev;

impl Cmd for Rev {
    fn cli(&self) -> clap::Command {
        Command::new("rev")
            .about("reverse lines characterwise")
            .author("Nathan Fisher")
            .args([args::header(), args::color(), args::file()])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        let color = match matches.get_one::<String>("color").map(String::as_str) {
            Some("always") => ColorChoice::Always,
            Some("ansi") => ColorChoice::AlwaysAnsi,
            Some("auto") => {
                if atty::is(atty::Stream::Stdout) {
                    ColorChoice::Auto
                } else {
                    ColorChoice::Never
                }
            }
            _ => ColorChoice::Never,
        };
        if let Some(files) = matches.get_many::<String>("file") {
            for (index, file) in files.enumerate() {
                if matches.get_flag("HEADER") {
                    let mut stdout = StandardStream::stdout(color);
                    stdout.set_color(ColorSpec::new().set_fg(Some(Color::Green)))?;
                    match index {
                        0 => writeln!(stdout, "===> {file} <==="),
                        _ => writeln!(stdout, "\n===> {file} <==="),
                    }?;
                    stdout.reset()?;
                }
                rev_file(file)?;
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}

fn rev_file(file: &str) -> Result<(), io::Error> {
    let reader: Box<dyn BufRead> = if file == "-" {
        Box::new(BufReader::new(io::stdin()))
    } else {
        let buf = File::open(file)?;
        Box::new(BufReader::new(buf))
    };
    for line in reader.lines() {
        println!("{}", line.unwrap().chars().rev().collect::<String>());
    }
    Ok(())
}
