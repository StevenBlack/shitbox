use super::Cmd;
use clap::Command;
use std::process;

#[derive(Debug)]
pub struct Nologin;

impl Cmd for Nologin {
    fn cli(&self) -> clap::Command {
        Command::new("nologin")
            .author("Nathan Fisher")
            .about("Denies a user account login ability")
    }

    fn run(&self, _matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        eprintln!("I'm sorry, I can't let you do that, Dave");
        process::exit(42);
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::Sbin)
    }
}
