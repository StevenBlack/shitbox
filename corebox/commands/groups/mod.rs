use super::Cmd;
use clap::{Arg, Command};

#[derive(Debug)]
pub struct Groups;

impl Cmd for Groups {
    fn cli(&self) -> clap::Command {
        Command::new("groups")
            .about("display current group names")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .after_help(
                "The groups command displays the current group names or ID values. \
                If the value does not have a corresponding entry in /etc/group, the \
                value will be displayed as the numerical group value. The optional \
                user parameter will display the groups for the named user.",
            )
            .arg(Arg::new("user"))
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        let groups = match matches.get_one::<String>("user") {
            Some(u) => pw::get_group_names_for_name(u)?,
            None => pw::get_group_names()?,
        };
        let len = groups.len();
        for (idx, group) in groups.into_iter().enumerate() {
            if idx < len - 1 {
                print!("{group} ");
            } else {
                println!("{group}");
            }
        }

        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
