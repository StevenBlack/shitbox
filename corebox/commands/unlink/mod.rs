use super::Cmd;
use clap::{Arg, Command};
use shitbox::args;

#[derive(Debug)]
pub struct Unlink;

impl Cmd for Unlink {
    fn cli(&self) -> clap::Command {
        Command::new("unlink")
            .about("call the unlink function to remove the specified file")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([
                args::verbose(),
                Arg::new("file")
                    .value_name("FILE")
                    .required(true)
                    .num_args(1..),
            ])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(files) = matches.get_many::<String>("file") {
            for f in files {
                unistd::unlink(f)?;
                if matches.get_flag("verbose") {
                    println!("unlink: {f}");
                }
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
