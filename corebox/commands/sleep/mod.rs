use super::Cmd;
use clap::{value_parser, Arg, ArgAction, ArgMatches, Command};
use std::{env, error::Error, thread, time::Duration};

#[derive(Debug)]
pub struct Sleep;

impl Cmd for Sleep {
    fn cli(&self) -> clap::Command {
        Command::new("sleep")
            .about("Suspend execution for an interval of time")
            .long_about(
                "The sleep utility suspends execution for a minimum of the specified number of seconds.\n\
                This number must be positive and may contain a decimal fraction.\n\
                sleep is commonly used to schedule the execution of other commands"
            )
            .author(env!("CARGO_PKG_AUTHORS"))
            .arg(
                Arg::new("seconds")
                    .help("The number of seconds to sleep")
                    .num_args(1)
                    .allow_negative_numbers(false)
                    .value_parser(value_parser!(f64))
                    .required(true)
                    .action(ArgAction::Set)
            )
    }

    #[allow(clippy::cast_sign_loss, clippy::cast_possible_truncation)]
    fn run(&self, matches: &ArgMatches) -> Result<(), Box<dyn Error>> {
        if let Some(raw) = matches.get_one::<f64>("seconds") {
            let seconds = *raw as u64;
            let nanos = ((raw % 1.0) * 10e-9) as u32;
            let s = Duration::new(seconds, nanos);
            thread::sleep(s);
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::Bin)
    }
}
