use {
    super::Cmd,
    clap::Command,
    libc::{geteuid, getpwuid},
    std::ffi::CStr,
};

#[derive(Debug)]
pub struct Whoami;

impl Cmd for Whoami {
    fn cli(&self) -> clap::Command {
        Command::new("whoami")
            .about("print effective user name")
            .author("Nathan Fisher")
    }

    fn run(&self, _matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        let pwnam = unsafe {
            let pw = *getpwuid(geteuid());
            CStr::from_ptr((pw).pw_name)
        };
        let pwnam = pwnam.to_str()?;
        println!("{pwnam}");
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
