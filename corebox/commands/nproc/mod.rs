use super::Cmd;
use clap::{Arg, ArgAction, Command};

#[derive(Debug)]
pub struct Nproc;

impl Cmd for Nproc {
    fn cli(&self) -> clap::Command {
        Command::new("nproc")
            .author("Nathan Fisher")
            .about("Print the number of processing units available")
            .arg(
                Arg::new("ALL")
                    .help("Print the number of installed processors")
                    .short('a')
                    .long("all")
                    .action(ArgAction::SetTrue),
            )
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if matches.get_flag("ALL") {
            println!("{}", unsafe { get_nprocs_conf() });
        } else {
            println!("{}", unsafe { get_nprocs() });
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}

extern "C" {
    fn get_nprocs() -> libc::c_int;
    fn get_nprocs_conf() -> libc::c_int;
}
