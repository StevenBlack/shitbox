use clap::ArgMatches;
use shitbox::Cmd;

mod base32;
mod base64;
mod basename;
mod bootstrap;
mod cat;
mod chmod;
mod chown;
mod chroot;
mod corebox;
mod cp;
mod cut;
mod date;
mod dd;
mod df;
mod dirname;
mod echo;
mod expand;
mod factor;
mod r#false;
mod fold;
mod getty;
mod groups;
mod head;
mod hostid;
mod hostname;
mod link;
mod ln;
mod logname;
mod ls;
mod mkfifo;
mod mknod;
mod mktemp;
mod mv;
mod nologin;
mod nproc;
mod printenv;
mod pwd;
mod readlink;
mod realpath;
mod rev;
mod rm;
mod rmdir;
mod sleep;
mod sync;
mod touch;
mod r#true;
mod truncate;
mod unlink;
mod wc;
mod which;
mod who;
mod whoami;
mod yes;

/// Parses a string into a command to run
#[must_use]
pub fn get(name: &str) -> Option<Box<dyn Cmd>> {
    match name {
        "base64" => Some(Box::new(base64::Base64)),
        "base32" => Some(Box::new(base32::Base32)),
        "basename" => Some(Box::new(basename::Basename)),
        "bootstrap" => Some(Box::new(bootstrap::Bootstrap)),
        "chmod" => Some(Box::new(chmod::Chmod)),
        "chgrp" => Some(Box::new(chown::Chgrp)),
        "chown" => Some(Box::new(chown::Chown)),
        "chroot" => Some(Box::new(chroot::Chroot)),
        "corebox" => Some(Box::new(corebox::Corebox)),
        "cut" => Some(Box::new(cut::Cut)),
        "df" => Some(Box::new(df::Df)),
        "dirname" => Some(Box::new(dirname::Dirname)),
        "echo" => Some(Box::new(echo::Echo)),
        "factor" => Some(Box::new(factor::Factor)),
        "false" => Some(Box::new(r#false::False)),
        "fold" => Some(Box::new(fold::Fold)),
        "groups" => Some(Box::new(groups::Groups)),
        "head" => Some(Box::new(head::Head)),
        "hostid" => Some(Box::new(hostid::Hostid)),
        "hostname" => Some(Box::new(hostname::Hostname)),
        "link" => Some(Box::new(link::Link)),
        "ln" => Some(Box::new(ln::Ln)),
        "logname" => Some(Box::new(logname::Logname)),
        "mkfifo" => Some(Box::new(mkfifo::MkFifo)),
        "mknod" => Some(Box::new(mknod::MkNod)),
        "mktemp" => Some(Box::new(mktemp::MkTemp)),
        "nologin" => Some(Box::new(nologin::Nologin)),
        "nproc" => Some(Box::new(nproc::Nproc)),
        "printenv" => Some(Box::new(printenv::Printenv)),
        "pwd" => Some(Box::new(pwd::Pwd)),
        "readlink" => Some(Box::new(readlink::Readlink)),
        "realpath" => Some(Box::new(realpath::Realpath)),
        "rev" => Some(Box::new(rev::Rev)),
        "rm" => Some(Box::new(rm::Rm)),
        "rmdir" => Some(Box::new(rmdir::Rmdir)),
        "sleep" => Some(Box::new(sleep::Sleep)),
        "sync" => Some(Box::new(sync::Sync)),
        "touch" => Some(Box::new(touch::Touch)),
        "true" => Some(Box::new(r#true::True)),
        "truncate" => Some(Box::new(truncate::Truncate)),
        "unlink" => Some(Box::new(unlink::Unlink)),
        "wc" => Some(Box::new(wc::Wc)),
        "which" => Some(Box::new(which::Which)),
        "who" => Some(Box::new(who::Who)),
        "whoami" => Some(Box::new(whoami::Whoami)),
        "yes" => Some(Box::new(yes::Yes)),
        _ => None,
    }
}

pub static COMMANDS: [&str; 46] = [
    "base32",
    "base64",
    "basename",
    "bootstrap",
    "chgrp",
    "chmod",
    "chown",
    "chroot",
    "corebox",
    "cut",
    "df",
    "dirname",
    "echo",
    "false",
    "factor",
    "fold",
    "groups",
    "head",
    "hostid",
    "hostname",
    "link",
    "ln",
    "logname",
    "mkfifo",
    "mknod",
    "mktemp",
    "nologin",
    "nproc",
    "printenv",
    "pwd",
    "readlink",
    "realpath",
    "rev",
    "rm",
    "rmdir",
    "sleep",
    "sync",
    "touch",
    "true",
    "truncate",
    "unlink",
    "wc",
    "which",
    "who",
    "whoami",
    "yes",
];

#[derive(Clone, Copy, Debug)]
enum Feedback {
    Full,
    Changes,
}

impl Feedback {
    fn from_matches(matches: &ArgMatches) -> Option<Self> {
        if matches.get_flag("verbose") {
            Some(Feedback::Full)
        } else if matches.get_flag("changes") {
            Some(Feedback::Changes)
        } else {
            None
        }
    }
}
