use std::ffi::CStr;

use super::Cmd;
use clap::Command;

#[derive(Debug)]
pub struct Logname;

impl Cmd for Logname {
    fn cli(&self) -> clap::Command {
        Command::new("logname")
            .about("print user's login name")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
    }

    fn run(&self, _matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        let logname = unsafe { CStr::from_ptr(libc::getlogin()) };
        let logname = logname.to_str()?;
        println!("{logname}");
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
