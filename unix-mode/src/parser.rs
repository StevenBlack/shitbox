use super::{get_umask, Bit, Who};
use bitflags::BitFlags;
use std::{error, fmt::Display, num::ParseIntError};

/// Errors which might occur when parsing Unix permissions from a string
#[derive(Debug, PartialEq)]
pub enum ParseError {
    /// the given `Bit` cannot be set for the given `Who`
    InvalidBit,
    /// the character is not recognized
    InvalidChar,
    /// the specified octal mode is invalid
    OutsideRange,
    ParseIntError(ParseIntError),
    /// no `Op` is set when parsing a `Bit`
    NoOpSet,
}

impl Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}

impl error::Error for ParseError {}

impl From<ParseIntError> for ParseError {
    fn from(value: ParseIntError) -> Self {
        Self::ParseIntError(value)
    }
}

#[derive(PartialEq)]
/// Operations which can be performed to add, remove, or set explicitly the given
/// `Bit` for the given `Who`
enum Op {
    /// `Bit`s will be added for `Who`
    Add,
    /// `Bit`s will be remoed for `Who`
    Remove,
    /// `Bit`s will be set to exactly the specified arrangement
    Equals,
}

/// A parser for octal and symbolic permissions. `Parser::default` creates an
/// instance which applies the given operations to the default setting for the
/// current user's umask. `Parser::new` creates a parser which applies the given
/// operations to the specified beginning set of permissions. Therefore, when
/// creating new files or directories use `Parser::default`, while `Parser::new`
/// should be used for setting permissions of already existing files or directories.
pub struct Parser {
    mode: u32,
    op: Option<Op>,
    who: u32,
    bits: u32,
}

impl Default for Parser {
    fn default() -> Self {
        let umask = get_umask();
        let mut mode = 0o0666;
        mode &= umask;
        Self {
            mode,
            op: None,
            who: 0,
            bits: 0,
        }
    }
}

impl Parser {
    #[must_use]
    pub fn new(mode: u32) -> Self {
        Self {
            mode,
            op: None,
            who: 0,
            bits: 0,
        }
    }

    fn parse_octal(value: &str) -> Result<u32, ParseError> {
        let m = u32::from_str_radix(value, 8)?;
        if m <= 0o7777 {
            Ok(m)
        } else {
            Err(ParseError::OutsideRange)
        }
    }

    fn add_who(&mut self, who: Who) -> Result<(), ParseError> {
        if self.op.is_some() || !self.bits == 0 {
            Err(ParseError::InvalidChar)
        } else {
            self.who |= who;
            Ok(())
        }
    }

    fn set_op(&mut self, op: Op) -> Result<(), ParseError> {
        if self.op.is_some() || !self.bits == 0 {
            Err(ParseError::InvalidChar)
        } else {
            self.op = Some(op);
            if self.who == 0 {
                self.who |= 0o111;
            }
            Ok(())
        }
    }

    fn push_read_bits(&mut self) -> Result<(), ParseError> {
        if self.op.is_none() {
            Err(ParseError::NoOpSet)
        } else {
            if self.who.contains(Who::User) {
                self.bits |= Bit::URead;
            }
            if self.who.contains(Who::Group) {
                self.bits |= Bit::GRead;
            }
            if self.who.contains(Who::Other) {
                self.bits |= Bit::ORead;
            }
            Ok(())
        }
    }

    fn push_write_bits(&mut self) -> Result<(), ParseError> {
        if self.op.is_none() {
            Err(ParseError::NoOpSet)
        } else {
            if self.who.contains(Who::User) {
                self.bits |= Bit::UWrite;
            }
            if self.who.contains(Who::Group) {
                self.bits |= Bit::GWrite;
            }
            if self.who.contains(Who::Other) {
                self.bits |= Bit::OWrite;
            }
            Ok(())
        }
    }

    fn push_exec_bits(&mut self) -> Result<(), ParseError> {
        if self.op.is_none() {
            Err(ParseError::NoOpSet)
        } else {
            if self.who.contains(Who::User) {
                self.bits |= Bit::UExec;
            }
            if self.who.contains(Who::Group) {
                self.bits |= Bit::GExec;
            }
            if self.who.contains(Who::Other) {
                self.bits |= Bit::OExec;
            }
            Ok(())
        }
    }

    fn push_suid_sgid(&mut self) -> Result<(), ParseError> {
        if self.who == 0 || self.who.contains(Who::Other) {
            return Err(ParseError::InvalidBit);
        } else if self.op.is_none() {
            return Err(ParseError::NoOpSet);
        }
        if self.who.contains(Who::User) {
            self.bits |= Bit::Suid;
        }
        if self.who.contains(Who::Group) {
            self.bits |= Bit::Sgid;
        }
        Ok(())
    }

    fn push_sticky(&mut self) -> Result<(), ParseError> {
        if self.who == 0 || self.who.contains(Who::User) || self.who.contains(Who::Group) {
            return Err(ParseError::InvalidBit);
        } else if self.op.is_none() {
            return Err(ParseError::NoOpSet);
        }
        if self.who.contains(Who::Other) {
            self.bits |= Bit::Sticky;
        }
        Ok(())
    }

    fn add_bits(&mut self) {
        self.mode |= self.bits;
    }

    fn remove_bits(&mut self) {
        self.mode &= !self.bits;
    }

    fn set_bits(&mut self) -> Result<(), ParseError> {
        match self.op {
            Some(Op::Add) => self.add_bits(),
            Some(Op::Remove) => self.remove_bits(),
            Some(Op::Equals) => {
                if self.who.contains(Who::User) {
                    self.mode &= !(0o4700);
                }
                if self.who.contains(Who::Group) {
                    self.mode &= !(0o2070);
                }
                if self.who.contains(Who::Other) {
                    self.mode &= !(0o1007);
                }
                self.add_bits();
            }
            None => return Err(ParseError::NoOpSet),
        }
        Ok(())
    }

    fn reset(&mut self) {
        self.who = 0;
        self.op = None;
        self.bits = 0;
    }

    #[must_use]
    pub fn mode(&self) -> u32 {
        self.mode
    }

    /// Parses a numerical mode from either an octal string or symbolic representation
    /// and applies those permissions to the starting set of permissions.
    /// # Errors
    /// Returns `ParseError` if:
    /// - invalid digit
    /// - no operation specified
    /// - more than one operation specified at a time (multiple operations can
    ///   be specified separated by comma)
    /// - the specified bit cannot be applied to the specified `Who`
    /// - bits are specified before operations
    /// - the specified octal mode is greater than 0o7777
    pub fn parse(&mut self, value: &str) -> Result<u32, ParseError> {
        match Self::parse_octal(value) {
            Ok(mode) => {
                self.mode = mode;
                return Ok(mode);
            }
            Err(e) => {
                if e == ParseError::OutsideRange {
                    return Err(e);
                }
            }
        }
        for c in value.chars() {
            match c {
                'u' => self.add_who(Who::User)?,
                'g' => self.add_who(Who::Group)?,
                'o' => self.add_who(Who::Other)?,
                'a' => {
                    self.add_who(Who::User)?;
                    self.add_who(Who::Group)?;
                    self.add_who(Who::Other)?;
                }
                '-' => self.set_op(Op::Remove)?,
                '+' => self.set_op(Op::Add)?,
                '=' => self.set_op(Op::Equals)?,
                'r' => self.push_read_bits()?,
                'w' => self.push_write_bits()?,
                'x' => self.push_exec_bits()?,
                's' => self.push_suid_sgid()?,
                't' => self.push_sticky()?,
                ',' => {
                    self.set_bits()?;
                    self.reset();
                }
                _ => return Err(ParseError::InvalidChar),
            }
        }
        self.set_bits()?;
        self.reset();
        Ok(self.mode)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn add() {
        let mut parser = Parser::new(0o644);
        let mode = parser.parse("ug+x");
        assert_eq!(mode, Ok(0o754));
    }

    #[test]
    fn remove() {
        let mut parser = Parser::new(0o777);
        let mode = parser.parse("go-wx");
        assert_eq!(mode, Ok(0o744));
    }

    #[test]
    fn octal() {
        let mut parser = Parser::default();
        let mode = parser.parse("4755");
        assert_eq!(mode, Ok(0o4755));
    }

    #[test]
    fn equals() {
        let mut parser = Parser::new(0o701);
        let mode = parser.parse("ugo=rw");
        assert_eq!(mode, Ok(0o666));
    }

    #[test]
    fn o_equals() {
        let mut parser = Parser::new(0o752);
        let mode = parser.parse("o=rx");
        assert_eq!(mode, Ok(0o755));
    }

    #[test]
    fn empty_who() {
        let mut parser = Parser::new(0o644);
        let mode = parser.parse("+x");
        assert_eq!(mode, Ok(0o755));
    }

    #[test]
    fn compound_ops() {
        let mut parser = Parser::new(0o666);
        let mode = parser.parse("u+x,g-w,o-r");
        assert_eq!(mode, Ok(0o742));
    }

    #[test]
    fn compount_ops2() {
        let mut parser = Parser::new(0o4444);
        let mode = parser.parse("u+w,ug+x,o=rx,u-s");
        assert_eq!(mode, Ok(0o755));
    }

    #[test]
    fn invalid_sticky_bit() {
        let mut parser = Parser::default();
        let mode = parser.parse("u+t");
        assert_eq!(mode, Err(ParseError::InvalidBit));
    }

    #[test]
    fn invalid_s() {
        let mut parser = Parser::default();
        let mode = parser.parse("+s");
        assert_eq!(mode, Err(ParseError::InvalidBit));
    }

    #[test]
    fn outside_range() {
        let mut parser = Parser::default();
        let mode = parser.parse("10000");
        assert_eq!(mode, Err(ParseError::OutsideRange))
    }

    #[test]
    fn no_op() {
        let mut parser = Parser::default();
        let mode = parser.parse("rws");
        assert_eq!(mode, Err(ParseError::NoOpSet));
    }

    #[test]
    fn ordering_error() {
        let mut parser = Parser::default();
        let mode = parser.parse("ux+s");
        assert_eq!(mode, Err(ParseError::NoOpSet));
    }

    #[test]
    fn ordering_error1() {
        let mut parser = Parser::default();
        let mode = parser.parse("x+s");
        assert_eq!(mode, Err(ParseError::NoOpSet));
    }
}
