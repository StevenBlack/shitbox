use super::COMMANDS;
use clap::Command;
use shitbox::Cmd;
use std::{error::Error, process};

#[derive(Debug, Default)]
pub struct Hashbox;

impl Cmd for Hashbox {
    fn cli(&self) -> clap::Command {
        let subcommands: Vec<Command> = {
            let mut s = vec![];
            for c in COMMANDS {
                if c == "hashbox" {
                    continue;
                }
                if let Some(cmd) = crate::commands::get(c) {
                    s.push(cmd.cli());
                }
            }
            s
        };
        Command::new("hashbox")
            .about("The box store multitool of embedded Linux")
            .version(env!("CARGO_PKG_VERSION"))
            .propagate_version(true)
            .arg_required_else_help(true)
            .subcommand_value_name("APPLET")
            .subcommand_help_heading("APPLETS")
            .subcommands(&subcommands)
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn Error>> {
        if let Some((name, matches)) = matches.subcommand() {
            if let Some(command) = crate::commands::get(name) {
                if let Err(e) = command.run(matches) {
                    eprintln!("Error: {name}: {e}");
                    process::exit(1);
                }
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        None
    }
}
