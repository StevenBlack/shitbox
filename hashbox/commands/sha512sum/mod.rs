use super::Cmd;
use crate::hash::{self, HashType};
use clap::Command;
use sha2::{Digest, Sha512};
use shitbox::args;
use std::{io, process};

#[derive(Debug, Default)]
pub struct Sha512sum;

impl Cmd for Sha512sum {
    fn cli(&self) -> clap::Command {
        Command::new("sha512sum")
            .about("compute and check SHA1 message digest")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([args::check(), args::file()])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(files) = matches.get_many::<String>("file") {
            let mut erred = 0;
            for f in files {
                if matches.get_flag("check") {
                    if f == "-" {
                        return Err(
                            io::Error::new(io::ErrorKind::Other, "no file specified").into()
                        );
                    }
                    hash::check_sums(f, HashType::Sha512, &mut erred)?;
                } else {
                    let hasher = Sha512::new();
                    let s = hash::compute_hash(f, hasher)?;
                    println!("{s}  {f}");
                }
            }
            if erred > 0 {
                println!("sha512sum: WARNING: {erred} computed checksum did NOT match");
                process::exit(1);
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
