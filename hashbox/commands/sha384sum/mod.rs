use super::Cmd;
use crate::hash::{self, HashType};
use clap::Command;
use sha2::{Digest, Sha384};
use shitbox::args;
use std::{io, process};

#[derive(Debug, Default)]
pub struct Sha384sum;

impl Cmd for Sha384sum {
    fn cli(&self) -> clap::Command {
        Command::new("sha384sum")
            .about("compute and check SHA1 message digest")
            .author("Nathan Fisher")
            .version(env!("CARGO_PKG_VERSION"))
            .args([args::check(), args::file()])
    }

    fn run(&self, matches: &clap::ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
        if let Some(files) = matches.get_many::<String>("file") {
            let mut erred = 0;
            for f in files {
                if matches.get_flag("check") {
                    if f == "-" {
                        return Err(
                            io::Error::new(io::ErrorKind::Other, "no file specified").into()
                        );
                    }
                    hash::check_sums(f, HashType::Sha384, &mut erred)?;
                } else {
                    let hasher = Sha384::new();
                    let s = hash::compute_hash(f, hasher)?;
                    println!("{s}  {f}");
                }
            }
            if erred > 0 {
                println!("sha384sum: WARNING: {erred} computed checksum did NOT match");
                process::exit(1);
            }
        }
        Ok(())
    }

    fn path(&self) -> Option<shitbox::Path> {
        Some(shitbox::Path::UsrBin)
    }
}
