//! An `Iterator` of the entries in /etc/fstab of /proc/mounts
use super::MntEntry;
use std::{
    fs::File,
    io::{self, BufRead, BufReader, Lines},
    path::Path,
};

/// An `Iterator` of the entries in /etc/fstab of /proc/mounts
pub struct MntEntries {
    lines: Lines<BufReader<File>>,
}

impl MntEntries {
    /// Create a new `Iterator` over the mounts specified in the file `p`
    pub fn new<P: AsRef<Path>>(p: P) -> io::Result<Self> {
        let fd = File::open(p)?;
        let reader = BufReader::new(fd);
        Ok(Self {
            lines: reader.lines(),
        })
    }
}

impl Iterator for MntEntries {
    type Item = MntEntry;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(line) = self.lines.next() {
                if let Ok(line) = line {
                    if let Ok(entry) = line.parse() {
                        break Some(entry);
                    }
                }
            } else {
                break None;
            }
        }
    }
}
