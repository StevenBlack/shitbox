//! Common arguments to be re-used across multiple commands. Using these args
//! instead of implementing from scratch increases consistency across applets
#![allow(clippy::must_use_candidate)]
use clap::{Arg, ArgAction};

pub fn verbose() -> Arg {
    Arg::new("verbose")
        .help("output a diagnostic for every file processed")
        .short('v')
        .long("verbose")
        .action(ArgAction::SetTrue)
}

pub fn header() -> Arg {
    Arg::new("HEADER")
        .help(
            "Each file is preceded by a header consisting of the string \
            \"==> XXX <==\" where \"XXX\" is the name of the file.",
        )
        .short('v')
        .long("verbose")
        .action(ArgAction::SetTrue)
}

pub fn changes() -> Arg {
    Arg::new("changes")
        .help("report only when a change is made")
        .short('c')
        .long("changes")
        .conflicts_with("verbose")
        .action(ArgAction::SetTrue)
}

pub fn recursive() -> Arg {
    Arg::new("recursive")
        .help("operate on files and directories recursively")
        .short('R')
        .long("recursive")
        .action(ArgAction::SetTrue)
}

pub fn color() -> Arg {
    Arg::new("color")
        .short('c')
        .long("color")
        .value_parser(["always", "ansi", "auto", "never"])
}

pub fn check() -> Arg {
    Arg::new("check")
        .help("read checksums from the FILEs and check them")
        .short('c')
        .long("check")
        .action(ArgAction::SetTrue)
}

pub fn file() -> Arg {
    Arg::new("file")
        .value_name("FILE")
        .num_args(1..)
        .default_value("-")
        .hide_default_value(true)
}
