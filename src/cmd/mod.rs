use clap::ArgMatches;
use std::error::Error;

/// Defines a command or applet, it's cli interface, and it's installation directory
/// relative to the binary
pub trait Cmd: Sync {
    /// Defines the cli of the applet
    fn cli(&self) -> clap::Command;
    /// Runs the applet
    /// # Errors
    /// Bubbles up any errors to the caller
    fn run(&self, matches: &ArgMatches) -> Result<(), Box<dyn Error>>;
    /// Returns the path relative to the binary where the link to this applet
    /// will be installed
    fn path(&self) -> Option<crate::Path>;
}
